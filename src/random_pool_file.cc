/* Random pool file
 * Copyright Paco Arjonilla <pacoarjonilla@yahoo.es>
 * Licensed under GPLv3
 *
 * Uses a file as a source of random bytes and keeps
 * track of the bytes consumed.
 * Usable as a random number generator for the STL distributions in <random>.
 */
#include "random_pool_file.hh"

#include <filesystem>
#include <limits>


random_pool_file::result_type
random_pool_file::operator() ()
{
    result_type ret;
    ifile.read(reinterpret_cast<std::ifstream::char_type *>(&ret),
               sizeof(result_type));

    if (ifile.eof())
	throw std::filesystem::filesystem_error(
		"random_pool_file: pool exhausted",
		ifilename, std::make_error_code(std::io_errc::stream) );
    if (!ifile.good())
	throw std::filesystem::filesystem_error(
		"random_pool_file: read error",
		ifilename, std::make_error_code(std::io_errc::stream) );

    return ret;
}

random_pool_file::random_pool_file(const std::string & filename)
    : ifilename(filename)
{
    ifile.open(ifilename, std::ios::in | std::ios::binary);

    if (!ifile.good())
	throw std::filesystem::filesystem_error(
		"random_pool_file: error: file does not exist",
		filename, std::make_error_code(std::io_errc::stream) );

    if (std::filesystem::exists(ifilename + ".bm")) {
	unsigned long bookmark;
	std::ifstream fbookmark(ifilename + ".bm");
	fbookmark >> bookmark;
	ifile.seekg(bookmark);
    }

    if (!ifile.good())
	throw std::filesystem::filesystem_error(
		"random_pool_file: error: seek failed. "
                "Consider clearing the bookmark file '" + filename + ".bm'",
		ifilename, std::make_error_code(std::io_errc::stream) );
}

random_pool_file::~random_pool_file()
{
    unsigned long bookmark = ifile.tellg();

    ifile.close();

    std::ofstream fbookmark(ifilename + ".bm", std::ios::out | std::ios::trunc);
    fbookmark << bookmark << std::endl;
}

std::ostream &
operator << (std::ostream & out, random_pool_file & pool)
{
    unsigned long bookmark = pool.ifile.tellg();

    pool.ifile.seekg(0, std::ios_base::end);
    unsigned long size = pool.ifile.tellg();
    pool.ifile.seekg(bookmark);

    out << "random_pool_file: Consumed random bytes "
        << bookmark << " out of " << size << " bytes ( "
        << (float)bookmark/size*100 << " % ) in file "
        << pool.ifilename << std::endl;

    return out;
}

