/* Random pool file
 * Copyright Paco Arjonilla <pacoarjonilla@yahoo.es>
 * Licensed under GPLv3
 *
 * Uses a file as a source of random bytes and keeps
 * track of the bytes consumed.
 * Usable as a random number generator for the STL distributions in <random>.
 */
#ifndef RANDOM_POOL_FILE_HH
#define RANDOM_POOL_FILE_HH

#include <fstream>
#include <iostream>
#include <limits>


/** random_pool_file
 *
 *  This random generator takes its random values from a regular file.
 *  It starts reading from the byte indicated in a bookmark file.
 *  The bookmark file is created or updated when the generator is destroyed.
 */
class random_pool_file
{
private:
    std::ifstream ifile;
    const std::string ifilename;

public:
    random_pool_file(const std::string & filename);
    ~random_pool_file();

public:
    using result_type = unsigned char;

    static constexpr result_type min()
        { return std::numeric_limits<result_type>::min(); }
    static constexpr result_type max()
        { return std::numeric_limits<result_type>::max(); }
    result_type operator() ();

    friend std::ostream & operator << (
            std::ostream & out, random_pool_file & pool);
};

std::ostream & operator << (std::ostream & out, random_pool_file & pool);

#endif

