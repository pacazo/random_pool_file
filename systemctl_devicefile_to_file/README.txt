This directory contains binary files with random bits extracted from a True Random Generator model Quantis PCIe.

Please do not copy any file. Rather, move them to your desktop and ensure that they get deleted in the server.
They will be automatically regenerated.

Names and sizes of regenerated file are specified in random_pool_files.config .

