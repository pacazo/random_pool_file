#!/bin/sh

cd /srvfiles/shared/random_pool
if [ -f "random_pool_files.config" ]; then
  while read count filename; do
    if [ ! -f $filename ]; then
      echo Generating file $filename with $count random bytes.
      dd if=/dev/qrandom0 of=$filename bs=1 count=$count
      chmod a+r $filename
    fi
  done < random_pool_files.config
fi
