# Random pool file

Random number device compatible with the C++ STL random library.
Reads random numbers from a file. Keeps track of the used bytes between runs.
Throws when the bytes in the file are exhausted.

USAGE
-----

- Include the header file.
- Compile the source file.
- Link the object file.

Please use Gitlab project page for any communication.

